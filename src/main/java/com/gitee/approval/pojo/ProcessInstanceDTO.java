package com.gitee.approval.pojo;

import lombok.Data;

@Data
public class ProcessInstanceDTO {
	String processInstanceId;
	String processDeploymentId;
}
